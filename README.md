# praat-tools

This project aims to become a comprehensive Python library to read and write not only TextGrid files, but also other Praat
objects; specifically, Pitch Tiers, and Duration Tiers.
