.. praat-tools documentation master file, created by
   sphinx-quickstart on Wed Jun 14 06:52:03 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to praat-tools's documentation!
=======================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   api


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
